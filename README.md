# location-display 位置显示程序

#### 介绍
该仓库的内容为“基于百度地图的位置显示程序”，可以在百度地图上显示静态目标和动态目标。动态目标的更新采用HTTP POST方法或者简单的TCP通信实现。如下图所示，三色箭头为**动态目标**(称为marker)，红色气泡为**静态目标**(称为location)。动态目标的各项信息会显示与左侧的侧边栏中。
![img1](/images/img1.jpg)

#### 软件架构
该仓库分为服务端(`/server`)和开发人员客户端(`/client`)两个部分。服务端使用Node.js+Express+EJS编写，页面中使用了JQuery来辅助开发。开发人员客户端**仅供开发调试使用**，采用Python编写，使用了`requests`库模拟上传/下载文件、更新动态目标位置和查询动态目标位置的功能。

#### 安装教程

1.  安装Node.js；
2.  克隆本仓库到服务器，并进入`/server`文件夹；
3.  使用`npm install`命令安装项目所需的包；
4.  键入`npm run dev`以调试模式启动本项目；
5.  本项目默认占用`80`和`5249`端口，可在`/server/bin/www`中修改`httpPort`和`tcpPort`的值；
6.  **不建议将该项目用于生产环境。** 如果需要使本项目连续运行，可以使用`npm install -g pm2`来安装`pm2`进程管理工具，再通过`pm2 start bin/www`启动项目。

#### 使用说明：服务端

##### A. 首次使用
1.  先去百度地图开放平台申请一个JavaScript API的密钥(AccessKey, AK)；
2.  登录管理员账户(用户名`admin`，密码`admin#123`)，单击导航栏“设置”一项，进入如下界面；
![img2](/images/img2.jpg)
3.  输入申请到的AK，单击“设置”按钮，显示设置成功后，单击上方“主页”即可正常使用；
4.  如果不需要使用发送模拟器，可以取消选中图片中的复选框。

##### B. 动态目标更新
更新动态目标的位置有两种方法：

**方法1：** 通过TCP协议向服务器5249端口发送消息。

消息格式如下所示：
```
S|数字UID编号|纬度,N/S(北纬/南纬),精度,E/W(东经/西经),海拔,M,气压,P,X角速度,Y角速度,Z角速度,X加速度,Y加速度,Z加速度,模式标记(a/S/E/A)
```

例如：
```
S|1|32.1,N,118.5,E,36.7,M,101463.0,P,1.00,2.00,3.00,1.00,2.00,3.000,S
```

**方法2：** 通过HTTP协议向服务器URL`/api/update`发送POST请求。

请求体应当如下设置：
```
{
    'uid':数字UID编号,
    'newdata':要更新的内容,即TCP消息的第三部分
}
```
例如：
```
{
    'uid':1,
    'newdata':'32.1,N,118.5,E,36.7,M,101463.0,P, 1.00,2.00,3.00,1.00,2.00,3.000,S'
}
```

动态目标的方向无法手动设定，系统会根据两次位置的差异自动计算方向。

##### C. 消息转发
通过TCP协议更新动态目标时，支持数据转发。转发目标应当向服务器5249端口发送字母`B`，以开始接收转发消息；通过再次发送字母`D`，可以停止接收转发消息。转发消息的内容只是将首字母`S`改为`F`，其他内容不变。

##### D. 动态目标初始位置和静态目标位置的设定
单击导航栏“设置”一项进入的页面中，有相关内容的设置，单击按钮可以进入编辑页面。

![img3](/images/img3.jpg)

在上图中Name为名称（无实际作用），IconID表示箭头样式（支持0~4），UID为TCP通信时使用的编号，InitAngle为初始角度（12点方向为零度，顺时针为正），InitLng为初始经度（东经），InitLat为初始纬度（北纬），操作一栏的按钮为删除键（**一旦点击，会不经确认直接删除！**）。

![img4](/images/img4.jpg)

为了编辑静态目标，Name为名称（在地图tooltip上显示），Lng为经度（东经），Lat纬度（北纬），操作一栏的按钮为删除键（**一旦点击，会不经确认直接删除！**）。

##### E. 文件管理

本系统具备单一文件的管理功能。

管理员界面如下，可以下载该文件当前版本和历史版本，上传该文件的新版本（上传的文件会被自动重命名为“日期_时间.db”）。可以编辑已有文件的文件名或者删除之。

![img5](/images/img5.jpg)

普通用户界面如下，只能下载当前版本的文件。

![img6](/images/img6.jpg)

##### F. 用户管理

默认用户列表如下：
|用户名|密码|
|---|---|
|admin|admin#123|
|user001|user001123|
|user002|user002123|
|user003|user003123|

管理员用户(admin)可以在用户管理界面添加、删除用户（一旦点击，会不经确认直接删除！），或者修改他们的密码。为了放置admin账户出现问题，该界面不能删除或修改admin账户。界面如下：

![img7](/images/img7.jpg)

#### 使用说明：开发人员客户端

**对于一般用户，直接从浏览器访问即可，无需使用开发人员客户端！**
1.	启动服务器；
2.	设置`config.ini`中的内容，即服务器地址，用户名，密码；
![img8](/images/img8.jpg)
3.	根目录(`/client`)下有4个python文件，分别是下载文件(test_download)、上传文件(test_upload)、更新动态目标(test_update)和查询动态目标位置(test_query)的示例代码。需要先安装requests库以实现http请求的发送。
