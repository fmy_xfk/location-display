const net = require('net');
const dataman = require('./routes/GPSinfo');

var count = 0;
var recv_sock = undefined;

function parse(data,cur_sock){
    let pdata=String(data).split("|");
    let op=pdata[0];
    if(op=='B'){ //设置转发socket
        recv_sock=cur_sock;
        console.log(`IP_recv=${recv_sock.remoteAddress}:${recv_sock.remotePort}`);
        return "@bind_ok";
    }else if(op=='S'){ //接收发来的消息，并转发至指定socket
        if(pdata.length>=3){
            var uid=Number(pdata[1]),msg=pdata[2];
            if(isNaN(uid)) return "@send_err";
            dataman.setrawdata(msg,uid);
            console.log(`uid=${uid},data="${msg}"`);
            if(recv_sock != undefined){
                data[0]=70; //F
                recv_sock.write(data);
            }
            return "@send_ok";
        }
        return "@send_err";
    }else if(op=='D'){ //关闭转发的socket
        if(recv_sock.remoteAddress===cur_sock.remoteAddress && recv_sock.remotePort===cur_sock.remotePort) {
            recv_sock = undefined;
            return "@down_ok";
        }
    }
    return "@uke_err";
}
// 创建TCP服务器
const tcpserver = new net.createServer();

tcpserver.setEncoding = 'UTF-8';
//获得一个连接，该链接自动关联scoket对象
tcpserver.on('connection', sock => {
    sock.name = `${sock.remoteAddress}:${sock.remotePort}`
    console.log(`当前连接客户端数：${++count}`);
    // 为这个socket实例添加一个"data"事件处理函数
    //客户端发送数据时触发data事件
    //接收client发来的信息  
    sock.on('data', data => {
        console.log(`from=${sock.name},data="${data}"`);
        // 回发该数据，客户端将收到来自服务端的数据
        sock.write(parse(data,sock));
        //sock.write(`服务端收到了你发送的一条信息：${data}`);
    });
    //为socket添加error事件处理函数
    sock.on('error', error => { //监听客户端异常
        console.log('error' + error);
        sock.destroy();
        //sock.end();
    });

    // 为这个socket实例添加一个"close"事件处理函数
    //当对方的连接断开以后的事件
    //服务器关闭时触发，如果存在连接，这个事件不会被触发，直到所有的连接关闭
    sock.on('close', () => {
        if(recv_sock==sock) {
            recv_sock=undefined;
        }
        console.log(`客户端${sock.name}下线了`);
        --count;
    });
})

module.exports = tcpserver;

