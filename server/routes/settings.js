var express = require('express');
var createError = require('http-errors');
var musr = require('./users');
var mapi = require('./api');
var router = express.Router();

router.get('/', function (req, res, next) {
    res.redirect('/settings/profile');
});

router.use(function (req, res, next) {
    if (musr.isLogined(req)) {
        next();
    } else {
        next(createError(403));
    }
});

router.get('/general', function (req, res, next) {
    res.render('settings/index', {
        "user": musr.getUser(req),
        "item_name": "general",
        "ak":mapi.get_ak(),
        "show_sim":mapi.get_show_sim()
    });
});

router.get('/profile', function (req, res, next) {
    res.render('settings/index', {
        "user": musr.getUser(req),
        "item_name": "profile",
    });
});

router.use(function (req, res, next) {
    if (musr.isAdmin(req)) {
        next();
    } else {
        next(createError(403));
    }
});

router.get('/locations', function (req, res, next) {
    res.render('settings/index', {
        "user": musr.getUser(req),
        "item_name": "locations",
        "te_items": mapi.get_locations()
    });
});

router.get('/markers', function (req, res, next) {
    res.render('settings/index', {
        "user": musr.getUser(req),
        "item_name": "markers",
        "te_items": mapi.get_markers()
    });
});

router.get('/usersman', function (req, res, next) {
    res.render('settings/index', {
        "user": musr.getUser(req),
        "item_name": "usersman",
        "userlist": musr.allUsers()
    });
});

module.exports = router;