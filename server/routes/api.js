var express = require('express');
const dataman = require('./GPSinfo')
const fs = require('fs');
const musr = require('./users');
var router = express.Router();

router.use((req, res, next) => {
    if (musr.isLogined(req)) {
        next();
    } else {
        res.send({ "success": false, "err": "NO_LOGIN" });
    }
});

router.post('/update', function (req, res, next) {
    var uid = Number(req.body.uid), data = req.body.newdata;
    if (req.body.uid === undefined || uid < 0 || uid > 65535) {
        res.send({ "success": false, "err": "INVALID_UID" });
        return;
    }
    if (data === undefined) {
        res.send({ "success": false, "err": "UNDEF_DATA" });
        return;
    }
    res.send(dataman.setrawdata(data, uid));
});

router.get('/query', function (req, res, next) {
    var uid_str = req.query.uid;
    if (uid_str === undefined || uid_str.trim() === '') {
        res.send({ "success": false, "err": "EMPTY_PAR" });
    } else {
        var ret;
        try {
            ret = dataman.query(uid_str.split(','));
            if (!ret.success) {
                res.send({ "success": false, "err": "QUERY_INTERNAL_ERR" });
                return;
            }
        } catch {
            res.send({ "success": false, "err": "QUERY_ERR" });
            return;
        }
        res.send({ "success": true, "uids": uid_str, "data": ret.data });
    }
});

CONFIG_FILE = __dirname + '/config.json';
AK_FILE = __dirname + '/ak.json';
var markers, locations, ak, show_sim;
function reloadConfig() {
    try {
        data = JSON.parse(fs.readFileSync(CONFIG_FILE));
    } catch {
        markers = locations = undefined;
        show_sim = true;
        return false;
    }
    try {
        markers = data["markers"];
    } catch {
        markers = undefined;
    }
    try {
        locations = data["locations"];
    } catch {
        locations = undefined;
    }
    try{
        show_sim = data["show_sim"];
    }catch{
        show_sim = true;
    }
    try {
        data = JSON.parse(fs.readFileSync(AK_FILE));
    } catch {
        ak = "";
        return false;
    }
    try {
        ak = data["ak"];
    } catch {
        ak = "";
    }
    return markers != undefined && locations != undefined && ak != "";
}
reloadConfig();
router.reloadConfig = reloadConfig;

function saveConfig() {
    try {
        fs.writeFileSync(CONFIG_FILE, JSON.stringify({ "markers": markers, "locations": locations }));
        fs.writeFileSync(AK_FILE, JSON.stringify({ "ak": ak }));
        return true;
    } catch {
        return false;
    }
}
router.saveConfig = saveConfig;

router.get('/markers', function (req, res, next) {
    if (markers === undefined) {
        res.send({ "success": false, "err": "INTERNAL_ERR" })
    } else {
        res.send({ "success": true, "data": markers });
    }
});

router.get_markers = () => { return markers; }

router.get('/locations', function (req, res, next) {
    if (locations === undefined) {
        res.send({ "success": false, "err": "INTERNAL_ERR" })
    } else {
        res.send({ "success": true, "data": locations });
    }
});

router.get_locations = () => { return locations; }

router.get('/ak', function (req, res, next) {
    if (ak === undefined) {
        res.send({ "success": false, "err": "INTERNAL_ERR" })
    } else {
        res.send({ "success": true, "data": ak });
    }
});

router.get_ak = () => { return ak; }

router.get_show_sim = () => { return show_sim; }

router.use((req, res, next) => {
    if (musr.isAdmin(req)) {
        next();
    } else {
        res.send({ "success": false, "err": "NOT_ADMIN" });
    }
});

router.post('/markers', function (req, res, next) {
    if (req.body.json != undefined) {
        markers = JSON.parse(req.body.json);
        res.send({ "success": saveConfig(), "err": "" });
    } else {
        res.send({ "success": false, "err": "empty" });
    }
});

router.post('/locations', function (req, res, next) {
    if (req.body.json != undefined) {
        locations = JSON.parse(req.body.json);
        res.send({ "success": saveConfig(), "err": "" });
    } else {
        res.send({ "success": false, "err": "empty" });
    }
});

router.post('/general', function (req, res, next) {
    if (req.body.ak != undefined) {
        ak = req.body.ak;
        show_sim = req.body.show_sim;
        res.send({ "success": saveConfig(), "err": "" });
    } else {
        res.send({ "success": false, "err": "empty" });
    }

});

module.exports = router;
