var express = require('express');
var musr = require('./users');
var mapi = require('./api')
var router = express.Router();

router.get('/', function (req, res, next) {
  if (musr.isLogined(req)) {
    res.render('index', { 
      "user": musr.getUser(req), 
      "ak": mapi.get_ak(), 
      "marker_cnt": mapi.get_markers().length,
      "show_sim": mapi.get_show_sim()
    });
  } else {
    res.render('index2', { "user": musr.getUser(req) });
  }
});

module.exports = router;
