//打开登录框时，清空登录框的内容
$('body').on('hidden.bs.modal', '.modal', function () {
    $(".modal input").val("");
    $(".my_tip").hide();
});

//隐藏提示条
function HideAlert() {
    $('#major-alert').hide();
}

//显示提示条3秒
function ShowAlert(content, type = 'info') {//type=primary,secondary,info,danger,success,warning
    $('#major-alert #content').text(content);
    $('#major-alert').attr('class', 'alert alert-' + type).show();
    setTimeout(HideAlert, 3000);
}

//前端发起登录请求
function login() {
    $.ajax({
        async: true, cache: false, type: 'POST', url: '/auth/login', data: { "username": $("#usr")[0].value, "password": $("#pwd")[0].value },
        success: function (data) {
            if (data.success) {
                location.reload();
            } else {
                if (data.err == 'bad user or pwd') {
                    $("#login_fail_tip").show();
                } else {
                    $("#login_elsewhere_tip").show();
                }
            }
        }
    });
}

//前端发起登出请求
function logout() {
    $.ajax({
        async: true, cache: false, type: 'POST', url: '/auth/logout',
        success: function (data) {
            if (data.success) {
                location.reload();
            }
        }
    });
}