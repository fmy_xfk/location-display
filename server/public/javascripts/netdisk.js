//网盘管理器：可以忽略
function upload_file() {
    var obj = {
        "upload_single": $("#my_upload")[0].files[0]
    };
    var formData = new FormData();
    $.each(obj, function (key, value) {
        formData.append(key, value);
    });
    $.ajax({
        type: 'post',
        url: '/netdisk/upload?path=<%=path%>',
        cache: false,
        contentType: false,
        processData: false,
        data: formData,
        success: function (data) {
            if (data.success) {
                alert("上传成功!");
            } else {
                alert("上传失败!\r\n错误信息：" + data.err);
            }
            location.reload();
        }
    });
}
function del(path) {
    if (confirm("你是否要删除" + path + "?")) {
        $.post("/netdisk/del?path=" + path, function (data) {
            if (data.success) {
                alert("删除成功!");
            } else {
                alert("删除失败!");
            }
            location.reload();
        });
    }
}
function rename(name) {
    var fname = prompt("新的名称:");
    if (fname != "" && fname != null && fname != undefined) {
        $.post("/netdisk/rename?path=<%=path%>/" + name + "&new=<%=path%>/" + fname, function (data) {
            if (data.success) {
                alert("改名成功!");
            } else {
                alert("改名失败!");
            }
            location.reload();
        });
    }
}