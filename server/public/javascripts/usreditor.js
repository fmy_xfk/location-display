//用户编辑器：可以忽略
function swpanel(usr) {
    $('#pwd1-' + usr).hide();
    $('#pwd2-' + usr).show();
}
function unreg(usr) {
    $.ajax({
        async: true, cache: false, type: 'POST', url: '/auth/unreg', data: { "usr": usr },
        success: function (data) {
            if (data.success === true) {
                set_tip(true, "用户删除成功。");
                $("#tr-" + usr).remove()
            } else {
                set_tip(false, "用户删除失败！");
            }
        },
        error: function (err) {
            set_tip(false, "用户删除失败！");
        }
    });
}
function register() {
    let usr = $("#new_user").val(), pwd = $("#new_pwd").val();
    $.ajax({
        async: true, cache: false, type: 'POST', url: '/auth/register', data: { "usr": usr, "pwd": pwd },
        success: function (data) {
            if (data.success === true) {
                location.reload();
            } else {
                set_tip(false, "用户创建失败！");
            }
        },
        error: function (err) {
            set_tip(false, "用户创建失败！");
        }
    });
}
function setpwd(usr) {
    let newpwd = $("#pwd-" + usr).val();
    $.ajax({
        async: true, cache: false, type: 'POST', url: '/auth/setpwd', data: { "usr": usr, "pwd": newpwd },
        success: function (data) {
            if (data.success === true) {
                set_tip(true, "密码修改成功。");
                $("#pwd-" + usr).val("");
                $('#pwd1-' + usr).show();
                $('#pwd2-' + usr).hide();
            } else {
                set_tip(false, "密码修改失败！");
            }
        },
        error: function (err) {
            set_tip(false, "密码修改失败！");
        }
    });
}

function set_tip(success, content) {
    target = "#te_tip";
    if (success === true) {
        $(target).removeClass("text-danger").addClass("text-success")
    } else {
        $(target).addClass("text-danger").removeClass("text-success")
    }
    $(target).text(content)
    $(target).show()
}
