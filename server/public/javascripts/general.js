//百度地图AK设置模块
SUCCESS_TEXT = "设置成功。";
FAILURE_TEXT = "设置失败。";
EMPTY_TEXT = "AK不能为空。";

function set_ak_tip(success, content) {
    const target = document.getElementById("setak_tip");
    if (success === true) {
        target.classList.remove("text-danger");
        target.classList.add("text-success");
    } else {
        target.classList.remove("text-success");
        target.classList.add("text-danger");
    }
    target.innerText = content;
    target.style.display = "block";
}

function submit_general() {
    let new_ak = document.getElementById("akbox").value;
    if (new_ak === '') {
        set_ak_tip(false, EMPTY_TEXT);
        return;
    }
    fetch('/api/general', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ 
            ak: new_ak, 
            show_sim: document.getElementById("showsim").checked
        })
    }).then(response => response.json())
    .then(data => {
        if (data.success === true) {
            set_ak_tip(true, SUCCESS_TEXT);
        } else {
            throw new Error();
        }
    }).catch((error) => {
        set_ak_tip(false, FAILURE_TEXT);
    });
}