//前端发起修改密码
function submit_chpwd() {
    $(".my_tip").hide();
    if ($('#npwd')[0].value == $('#opwd')[0].value) {
        $("#chpwd_repeat").show();
        return;
    } else if ($('#npwd')[0].value != $('#cpwd')[0].value) {
        $("#chpwd_incon").show();
        return;
    } else if ($('#npwd')[0].value.length < 8) {
        $("#chpwd_short").show();
        return;
    }
    $.ajax({
        async: true, cache: false, type: 'POST', url: '/auth/chpwd',
        data: { "old": $('#opwd')[0].value, "new": $('#npwd')[0].value },
        success: function (data) {
            if (data.success === true) {
                $("#chpwd_ok").show();
            } else {
                $("#chpwd_fail").show();
            }
        },
        error: function (err) {
            $("#chpwd_uke").show();
        }
    });
}