//位置更新模拟器
function simSend() {
    let id = document.getElementById('sim-upd-id').value
    let con = document.getElementById('sim-upd-content').value
    fetch('/api/update', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json;charset=utf-8' },
        body: JSON.stringify({ 'uid': id, 'newdata': con })
    }).then(res => {
        return res.json();
    }).then(res => {
        document.getElementById('info-content').innerText = res['success'] ? "发送成功！" : "发送失败！";
    })
}