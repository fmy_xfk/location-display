from gclient import *

def m_update(uid,content):
    print("位置更新测试……")
    # 读配置
    conf=read_ini("config.ini")['gclient']
    cli=GPSClient(conf['url'],conf['user'],conf['pwd'])
    if(not cli.login()["success"]): 
        print("登录失败！无法测试。")
    ret=cli.update(uid,content)
    if(not ret["success"]):
        print("测试失败！")
        print(ret)
    else:
        print("测试成功")
    cli.logout()
    
if __name__=='__main__':
    m_update(1,"32.1,N,118.8,E,36.7,M,101352,P,1.00,2.00,3.00,1.00,2.00,3.000,S")