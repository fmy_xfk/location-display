from gclient import *

def m_upload():
    print("正在上传风机文件……")
    # 读配置
    conf=read_ini("config.ini")['gclient']
    cli=GPSClient(conf['url'],conf['user'],conf['pwd'])
    if(not cli.login()["success"]): 
        print("登录失败！无法上传。")
    fname="test_file.txt"
    ret=cli.upload(fname)
    if(not ret["success"]):
        print("上传失败！")
        print(ret)
    else:
        print("文件"+fname+"已成功上传")
    cli.logout()

if __name__=='__main__':
    m_upload()