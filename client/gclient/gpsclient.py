import requests

class GPSClient:
    def __init__(self,root_url:str,user:str,pwd:str):
        '''
        初始化。
        root_url: 服务器地址, 例如http://www.example.com。
        user: 用户名。
        pwd: 密码明文。请注意, 您的密码有可能被明文传送。
        '''
        self.host=root_url.rstrip('/\\')
        self.user=user
        self.pwd=pwd
        self.session=requests.session()
        self.logined=False
    
    def login(self,user:str=None,pwd:str=None):
        '''
        登录。请注意, 进行任何操作之前都应该现登录！
        user: 用户名。(可选)
        pwd: 密码明文。请注意, 您的密码有可能被明文传送。(可选)
        返回值: {"success":True/False,"err":"错误原因"}
        '''
        if(user!=None):self.user=user
        if(pwd!=None):self.pwd=pwd
        if(self.user==None or self.pwd==None): raise "BLANK_USER_OR_PWD"
        ret=self.session.post(self.host+'/auth/login',{"username":self.user,"password":self.pwd})
        if(ret.status_code>=400):
            self.logined=False
            return {"success":False,"err":ret.status_code}
        ret=ret.json()
        if(not ret["success"]):
            self.logined=False
            return ret
        self.logined=True
        return ret
    
    def update(self,uid:int,data:str):
        '''
        更新位置。如果没有登录会先尝试登录。
        uid: 0~65535, 表示要更新信息的标记的编号。
        data: 更新数据, 一个字符串, 格式形如32.117888,N,118.905159,E,36.758000,M,101463.000000,P,1.00,2.00,3.00,1.00,2.00,3.000,S
        返回值: {"success":True/False,"err":"错误原因"}
        '''
        if(not self.logined): 
            ret = self.login()
            if(not ret["success"]): return ret
        ret = self.session.post(self.host+"/api/update",data={"uid":uid,"newdata":data})
        if(ret.status_code>=400):
            return {"success":False,"err":ret.status_code}
        return ret.json()
    
    def query(self,uids):
        '''
        查询位置。如果没有登录会先尝试登录。
        uids: 一个数值列表, 表示要查询的所有标记的编号。
        返回值: {"success":True/False,"err":"错误原因"}
        '''
        if(not self.logined): 
            ret = self.login()
            if(not ret["success"]): return ret
        ret = self.session.get(self.host+'/api/query?uid='+','.join([str(s) for s in uids]))
        if(ret.status_code>=400):
            return {"success":False,"err":ret.status_code}
        return ret.json()

    def download(self,local_path:str):
        '''
        下载风机文件。如果没有登录会先尝试登录。
        local_path: 下载到的本地位置。
        返回值: {"success":True/False,"err":"错误原因"}
        '''
        try:
            fp = open(local_path,'wb')
        except:
            return {"success":False,"err":"FILE_CANNOT_OPEN"}
        try:
            ret=self.session.get(self.host+'/netdisk/dlcur')
        except:
            return {"success":False,"err":"FAIL_TO_DOWNLOAD"}
        #print(ret.text)
        fp.write(ret.content)
        fp.close()
        return {"success":True,"err":""}

    def upload(self,local_path:str):
        '''
        上传风机文件。如果没有登录会先尝试登录。
        local_path: 要上传的本地文件。
        返回值: {"success":True/False,"err":"错误原因"}
        '''
        try:
            fp = open(local_path,'rb')
        except:
            return {"success":False,"err":"FILE_CANNOT_OPEN"}
        try:
            ret=self.session.post(self.host+'/netdisk/upload?path=.',files={"upload_single":fp})
        except:
            return {"success":False,"err":"FAIL_TO_UPLOAD"}
        fp.close()
        if(ret.status_code>400):
            print(ret.text)
            return {"success":False,"err":ret.text}
        return ret.json()

    def logout(self):
        '''
        登出。
        '''
        if(not self.logined): 
            ret = self.login()
            if(not ret["success"]): return ret
        ret = self.session.post(self.host+'/auth/logout')
        if(ret.status_code>=400):
            return {"success":False,"err":ret.status_code}
        return ret.json()

if __name__=='__main__':
    print("GClient Library - Version 1.0")
    print("Don't run this directly. Thank you.")