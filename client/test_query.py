from gclient import *

def m_query():
    print("位置查询测试……")
    # 读配置
    conf=read_ini("config.ini")['gclient']
    cli=GPSClient(conf['url'],conf['user'],conf['pwd'])
    if(not cli.login()["success"]): 
        print("登录失败！无法测试。")
    ret=cli.query([1,2,3])
    if(not ret["success"]):
        print("测试失败！")
    else:
        print("测试成功")
    print(ret)
    cli.logout()
    
if __name__=='__main__':
    m_query()