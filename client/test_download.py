from gclient import *

def m_download(fname):
    print("正在下载风机文件……")
    # 读配置
    conf=read_ini("config.ini")['gclient']
    cli=GPSClient(conf['url'],conf['user'],conf['pwd'])
    if(not cli.login()["success"]): 
        print("登录失败！无法下载。")
    ret=cli.download(fname)
    if(not ret["success"]):
        print("下载失败！")
        print(ret)
    else:
        print("文件已下载至"+fname)
    cli.logout()

if __name__=='__main__':
    m_download("download.db")