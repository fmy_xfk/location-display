# location-display Location Display Program

#### Description
The content of this repository is "Baidu Map Based Location Display Program", which can display static and dynamic targets on Baidu Map. The update of dynamic targets is achieved using HTTP POST method or simple TCP communication. As shown in the figure below, the three colored arrow represents the **dynamic target** (referred to as the marker), and the red bubble represents the **static target** (referred to as the location). The various information of dynamic targets will be displayed in the sidebar on the left.
![img1](/images/img1.jpg)

#### Software Architecture
The repository is divided into two parts: the server (`/server`) and the developer's client (`/client`). The server is constructed using Node.js + Express + EJS, and the front-end page uses JQuery to assist in development. The developer's client is **only for development and debugging purposes** and is developed by Python. It uses the `requests` library to simulate uploading/downloading files, updating dynamic target locations, and querying dynamic target locations.

#### Installation
1. Install Node.js;
2. Clone this repository to your server and enter the `/server` folder;
3. Use the `npm install` command to install the required packages for the project;
4. Type `npm run dev` to start this project in debugging mode;
5. This project defaults to using ports 80 and 5249, and the values of `httpPort` and `tcpPort` can be modified in `/server/bin/www`;
6. **It is not recommended to use this project for production environments.** If you need to make this project run continuously, you can use `npm install -g pm2` to install the `pm2` process management tool, and then start the project through `pm2 start bin/www`.